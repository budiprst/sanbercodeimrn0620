console.log(" ");
console.log("Soal A");
//SOAL A

function DescendingTen(num) {
  var strHasil = "";
  if (!num) {
    strHasil += "-1";
  } else {
    for (var i = 0; i < 10; i++) {
      strHasil += num - i + " ";
    }
  }
  return strHasil;
}

// TEST CASES Descending Ten
console.log(DescendingTen(100)); // 100 99 98 97 96 95 94 93 92 91
console.log(DescendingTen(10)); // 10 9 8 7 6 5 4 3 2 1
console.log(DescendingTen()); // -1

console.log(" ");
console.log("Soal B");
//SOAL B

function AscendingTen(num) {
  var strHasil = "";
  if (!num) {
    strHasil += "-1";
  } else {
    for (var i = 0; i < 10; i++) {
      strHasil += num + i + " ";
    }
  }
  return strHasil;
}

// TEST CASES Ascending Ten
console.log(AscendingTen(11)); // 11 12 13 14 15 16 17 18 19 20
console.log(AscendingTen(21)); // 21 22 23 24 25 26 27 28 29 30
console.log(AscendingTen()); // -1

console.log(" ");
console.log("Soal C");
//SOAL C

function ConditionalAscDesc(reference, check) {
  if (!check) {
    return -1;
  } else {
    if (check % 2 == 0) {
      //Genap - desc
      return DescendingTen(reference);
    } else {
      //Ganjil - asc
      return AscendingTen(reference);
    }
  }
}

// TEST CASES Conditional Ascending Descending
console.log(ConditionalAscDesc(20, 8)); // 20 19 18 17 16 15 14 13 12 11
console.log(ConditionalAscDesc(81, 1)); // 81 82 83 84 85 86 87 88 89 90
console.log(ConditionalAscDesc(31)); // -1
console.log(ConditionalAscDesc()); // -1

console.log(" ");
console.log("Soal D");
//SOAL D

function ularTangga() {
  var i = 100;
  var flip = false;
  while (i > 0) {
    if (flip == false) {
      console.log(DescendingTen(i));
      flip = true;
    } else {
      console.log(AscendingTen(i - 9));
      flip = false;
    }
    i -= 10;
  }
  return "";
}

// TEST CASE Ular Tangga
console.log(ularTangga());
/*
Output :
  100 99 98 97 96 95 94 93 92 91
  81 82 83 84 85 86 87 88 89 90
  80 79 78 77 76 75 74 73 72 71
  61 62 63 64 65 66 67 68 69 70
  60 59 58 57 56 55 54 53 52 51
  41 42 43 44 45 46 47 48 49 50
  40 39 38 37 36 35 34 33 32 31
  21 22 23 24 25 26 27 28 29 30
  20 19 18 17 16 15 14 13 12 11
  1 2 3 4 5 6 7 8 9 10
*/
