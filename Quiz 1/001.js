console.log(" ");
console.log("Soal A");
//SOAL A

function bandingkan(num1, num2) {
  //console.log(num1, num2);
  if (num1 < 0 || num2 < 0 || num1 == num2) {
    return -1;
  } else if (!num2) {
    return num1;
  } else if (num1 > num2) {
    return num1;
  } else {
    return num2;
  }
}

// TEST CASES Bandingkan Angka
console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1
console.log(bandingkan(112, 121)); // 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")); // 18

//note: kurang 1 yg belum beres

console.log(" ");
console.log("Soal B");
//SOAL B

function balikString(str) {
  //console.log(str.length);
  var max = str.length - 1;
  var hasil = "";
  for (var i = max; i >= 0; i--) {
    hasil += str[i];
    //console.log(str[i]);
  }
  return hasil;
}

// TEST CASES BalikString
console.log(balikString("abcde")); // edcba
console.log(balikString("rusak")); // kasur
console.log(balikString("racecar")); // racecar
console.log(balikString("haji")); // ijah

console.log(" ");
console.log("Soal C");
//SOAL C

function palindrome(str) {
  var x = balikString(str);
  if (x == str) {
    return true;
  } else {
    return false;
  }
}

// TEST CASES Palindrome
console.log(palindrome("kasur rusak")); // true
console.log(palindrome("haji ijah")); // true
console.log(palindrome("nabasan")); // false
console.log(palindrome("nababan")); // true
console.log(palindrome("jakarta")); // false
