console.log("Soal 1");
//Soal 1
console.log("LOOPING PERTAMA");
var flag = 1;
while (flag <= 10) {
  // Loop akan terus berjalan selama nilai flag masih dibawah 10
  console.log(flag * 2 + " - I love coding"); // Menampilkan nilai flag pada iterasi tertentu
  flag++; // Mengubah nilai flag dengan menambahkan 1
}
console.log("LOOPING KEDUA");
var flag = 10;
while (flag > 0) {
  // Loop akan terus berjalan selama nilai flag masih dibawah 10
  console.log(flag * 2 + " - I will become a mobile developer"); // Menampilkan nilai flag pada iterasi tertentu
  flag--; // Mengubah nilai flag dengan menambahkan 1
}

console.log("");
console.log("Soal 2");
//Soal 2
var output = "";
for (var angka = 1; angka <= 20; angka++) {
  if (angka % 2 == 1) {
    if (angka % 3 == 0) {
      output = "I Love Coding ";
    } else {
      output = "Santai";
    }
  } else {
    output = "Berkualitas";
  }
  //console.log(angka % 2);
  console.log(angka + " - " + output);
}

console.log("");
console.log("Soal 3");
//Soal 3
var col;
for (var row = 1; row <= 4; row++) {
  col = "";
  for (var column = 1; column <= 8; column++) {
    col += "#";
  }
  console.log(col);
}

console.log("");
console.log("Soal 4");
//Soal 4
var col;
for (var row = 1; row <= 7; row++) {
  col = "";
  for (var column = 1; column <= row; column++) {
    col += "#";
  }
  console.log(col);
}

console.log("");
console.log("Soal 5");
//Soal 5
var g;
for (var row = 1; row <= 8; row++) {
  col = "";
  g = 0;
  if (row % 2 == 0) {
    g = 1;
  }
  for (var column = 1 + g; column <= 8 + g; column++) {
    if (column % 2 == 0) {
      col += "#";
    } else {
      col += " ";
    }
  }
  console.log(col);
}
