console.log("");
console.log("Soal 1:");
//Soal 1

function arrayToObject(arr) {
  var obj = {};
  const tahun = new Date().getFullYear();
  var count = 1;
  arr.forEach((e) => {
    var umur;
    if (tahun < e[3] || !e[3]) {
      umur = "Invalid birth year";
    } else {
      umur = tahun - e[3];
    }
    var key = count + ". " + e[0] + " " + e[1];
    obj[key] = {
      firstname: e[0],
      lastname: e[1],
      gender: e[2],
      age: umur,
    };

    count++;
  });

  if (arr.length < 1) {
    console.log("");
  } else {
    console.log(obj);
  }
}

// Driver Code
var people = [
  ["Bruce", "Banner", "male", 1975],
  ["Natasha", "Romanoff", "female"],
];
arrayToObject(people);
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

var people2 = [
  ["Tony", "Stark", "male", 1980],
  ["Pepper", "Pots", "female", 2023],
];
arrayToObject(people2);
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

// Error case
arrayToObject([]); // ""

console.log("");
console.log("Soal 2:");
//Soal 2

function shoppingTime(memberId, money) {
  if (!memberId) {
    return "Mohon maaf, toko X hanya berlaku untuk member saja";
  } else if (money < 50000) {
    return "Mohon maaf, uang tidak cukup";
  } else {
    var dana = money;
    var listPurchased = [];
    if (dana >= 1500000) {
      listPurchased.push("Sepatu Stacattu");
      dana -= 1500000;
    }
    if (dana >= 500000) {
      listPurchased.push("Baju Zoro");
      dana -= 500000;
    }
    if (dana >= 250000) {
      listPurchased.push("Baju H&N");
      dana -= 250000;
    }
    if (dana >= 175000) {
      listPurchased.push("Sweater Uniklooh");
      dana -= 175000;
    }
    if (dana >= 50000) {
      listPurchased.push("Casing Handphone");
      dana -= 50000;
    }

    var objRes = {
      memberId: memberId,
      money: money,
      listPurchased: listPurchased,
      changeMoney: dana,
    };

    return objRes;
  }
}

// TEST CASES
console.log(shoppingTime("1820RzKrnWn08", 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime("82Ku8Ma742", 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime("", 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime("234JdhweRxa53", 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

console.log("");
console.log("Soal 3:");
//Soal 3

function naikAngkot(arrPenumpang) {
  rute = ["A", "B", "C", "D", "E", "F"];
  //your code here
  //console.log("start");
  if (arrPenumpang.length < 1) {
    return arrPenumpang;
  } else {
    var res = [];
    arrPenumpang.forEach((e) => {
      //console.log("start-loop");
      var biaya = 0;
      var s = e[1];
      while (s != e[2]) {
        biaya += 2000;
        s = String.fromCharCode(s.charCodeAt(0) + 1);
      }

      var obj = {
        penumpang: e[0],
        naikDari: e[1],
        tujuan: e[2],
        bayar: biaya,
      };
      //console.log(obj);
      res.push(obj);
    });

    return res;
  }
}

//TEST CASE
console.log(
  naikAngkot([
    ["Dimitri", "B", "F"],
    ["Icha", "A", "B"],
  ])
);
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]
