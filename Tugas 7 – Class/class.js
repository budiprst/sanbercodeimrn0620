console.log("");
console.log("Soal 1:");
//Soal 1
console.log("Release 0:");
//Release 0

class Animal {
  constructor(inp) {
    this.nameVar = inp;
    this.legsVar = 4;
    this.cold_bloodedVar = false;
  }

  //setter getter
  get name() {
    return this.nameVar;
  }
  set name(x) {
    this.nameVar = x;
  }

  get legs() {
    return this.legsVar;
  }
  set legs(x) {
    this.legsVar = x;
  }

  get cold_blooded() {
    return this.cold_bloodedVar;
  }
  set cold_blooded(x) {
    this.cold_bloodedVar = x;
  }
}

var sheep = new Animal("shaun");

console.log(sheep.name); // "shaun"
console.log(sheep.legs); // 4
console.log(sheep.cold_blooded); // false

console.log("Release 1:");
//Release 1

class Ape extends Animal {
  constructor(inp) {
    super(inp);
    this.legsVar = 2;
  }

  yell() {
    console.log("Auooo");
  }
}

class Frog extends Animal {
  constructor(inp) {
    super(inp);
  }

  jump() {
    console.log("hop hop");
  }
}

var sungokong = new Ape("kera sakti");
sungokong.yell(); // "Auooo"

var kodok = new Frog("buduk");
kodok.jump(); // "hop hop"

console.log("");
console.log("Soal 2:");
//Soal 2

class Clock {
  constructor({ template }) {
    this.template = template;
  }

  //var timer;

  render() {
    var date = new Date();

    var hours = date.getHours();
    if (hours < 10) hours = "0" + hours;

    var mins = date.getMinutes();
    if (mins < 10) mins = "0" + mins;

    var secs = date.getSeconds();
    if (secs < 10) secs = "0" + secs;

    var output = this.template
      .replace("h", hours)
      .replace("m", mins)
      .replace("s", secs);

    console.log(output);
  }

  stop = function () {
    clearInterval(this.timer);
  };

  start = function () {
    this.render();
    this.timer = setInterval(() => {
      this.render();
    }, 1000);
  };
}

var clock = new Clock({ template: "h:m:s" });
clock.start();
