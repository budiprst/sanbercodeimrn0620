/**
 * Berikut soal quiz kali ini, terdiri dari 3 Soal
 * Kerjakan dengan sebaik mungkin, dengan menggunakan metode yang telah dipelajari,
 * Tidak diperkenankan untuk menjawab hanya dengan console.log('teks jawaban');
 * maupun dengan terlebih dahulu memasukkannya ke dalam variabel, misal var a = 'teks jawaban'; console.log(a);
 *
 * Terdapat tambahan poin pada setiap soal yang dikerjakan menggunakan sintaks ES6 (+5 poin)
 * Jika total nilai Anda melebihi 100 (nilai pilihan ganda + coding), tetap akan memiliki nilai akhir yaitu 100
 *
 * Selamat mengerjakan
 */

/*========================================== 
  1. SOAL CLASS SCORE (10 poin + 5 Poin ES6)
  ==========================================
  Buatlah sebuah class dengan nama Score. class Score tersebut memiliki properti "subject", "points", dan "email". 
  "points" dapat di input berupa number (1 nilai) atau array of number (banyak nilai).
  tambahkan method average untuk menghitung rata-rata dari parameter points ketika yang di input berupa array (lebih dari 1 nilai)
*/

class Score {
  constructor(email, sbj, point) {
    this.email = email;
    this.subject = sbj;
    this.points = point;
  }

  average() {
    if (Array.isArray(this.points)) {
      let values = this.points;
      let sum = values.reduce((previous, current) => (current += previous));
      let avg = sum / values.length;
      return avg;
    } else {
      return this.points;
    }
  }
}

console.log("");
console.log("Soal 1:");
//Soal 1
console.log(new Score("", "", [2, 56, 3, 41, 0, 4, 100, 23]).average());
console.log(new Score("", "", 10).average(), "");

/*=========================================== 
  2. SOAL Create Score (10 Poin + 5 Poin ES6)
  ===========================================
  Membuat function viewScores yang menerima parameter data berupa array multidimensi dan subject berupa string
  Function viewScores mengolah data email dan nilai skor pada parameter array 
  lalu mengembalikan data array yang berisi object yang dibuat dari class Score.
  Contoh: 

  Input
   
  data : 
  [
    ["email", "quiz-1", "quiz-2", "quiz-3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88]
  ]
  subject: "quiz-1"

  Output 
  [
    {email: "abduh@mail.com", subject: "quiz-1", points: 78},
    {email: "khairun@mail.com", subject: "quiz-1", points: 95},
  ]
*/

const data = [
  ["email", "quiz - 1", "quiz - 2", "quiz - 3"],
  ["abduh@mail.com", 78, 89, 90],
  ["khairun@mail.com", 95, 85, 88],
  ["bondra@mail.com", 70, 75, 78],
  ["regi@mail.com", 91, 89, 93],
];

function viewScores(data, subject) {
  //console.log(data, subject);

  var index = 0;
  var n = -1;
  data.forEach((e) => {
    if (index == 0) {
    } else {
      switch (subject) {
        case "quiz-1": {
          n = 1;
          break;
        }
        case "quiz-2": {
          n = 2;
          break;
        }
        case "quiz-3": {
          n = 3;
          break;
        }
        default: {
          n = -1;
        }
      }

      if (n != -1) {
        let hasil = new Score(e[0], subject, e[n]);
        console.log(hasil);
      }
    }
    index++;
  });
}

console.log("");
console.log("Soal 2:");
//Soal 2
//
// TEST CASE
viewScores(data, "quiz-1");
viewScores(data, "quiz-2");
viewScores(data, "quiz-3");

/*==========================================
  3. SOAL Recap Score (15 Poin + 5 Poin ES6)
  ==========================================
    Buatlah fungsi recapScore yang menampilkan perolehan nilai semua student. 
    Data yang ditampilkan adalah email user, nilai rata-rata, dan predikat kelulusan. 
    predikat kelulusan ditentukan dari aturan berikut:
    nilai > 70 "participant"
    nilai > 80 "graduate"
    nilai > 90 "honour"

    output:
    1. Email: abduh@mail.com
    Rata-rata: 85.7
    Predikat: graduate

    2. Email: khairun@mail.com
    Rata-rata: 89.3
    Predikat: graduate

    3. Email: bondra@mail.com
    Rata-rata: 74.3
    Predikat: participant

    4. Email: regi@mail.com
    Rata-rata: 91
    Predikat: honour

*/

function recapScores(data) {
  var index = 0;

  data.forEach((e) => {
    if (index == 0) {
    } else {
      let email = e[0];
      let arr = e.shift();

      let hasil = new Score(e[0], "", e);
      let avg = Number(hasil.average().toFixed(1));
      let predikat = "-";
      if (avg > 70 && avg <= 80) {
        predikat = "participant";
      } else if (avg > 80 && avg <= 90) {
        predikat = "graduate";
      } else if (avg > 90) {
        predikat = "honour";
      }

      console.log(index + ". Email: " + email);
      console.log("Rata - rata: " + avg);
      console.log("Predikat: " + predikat);
      console.log("");
    }
    index++;
  });
}

console.log("");
console.log("Soal 3:");
//Soal 3
recapScores(data);
