console.log("Soal 1:");
//Soal 1
var nama = "Josh";
var peran = "Werewolf";

var output = "Nama harus diisi!";

if (nama == "" && peran == "") {
} else if (nama != "") {
  if (peran == "") {
    output = "Halo " + nama + ", Pilih peranmu untuk memulai game!";
  } else {
    console.log("Selamat datang di Dunia Werewolf, " + nama);
    if (peran == "Penyihir") {
      output =
        "Halo " +
        peran +
        " " +
        nama +
        ", kamu dapat melihat siapa yang menjadi werewolf!";
    } else if (peran == "Guard") {
      output =
        "Halo " +
        peran +
        " " +
        nama +
        ", kamu akan membantu melindungi temanmu dari serangan werewolf.";
    } else if (peran == "Werewolf") {
      output =
        "Halo " +
        peran +
        " " +
        nama +
        ", Kamu akan memakan mangsa setiap malam!";
    }
  }
}

console.log(output);

console.log("");
console.log("Soal 2:");
//Soal 2
var hari = 21;
var bulan = 6;
var tahun = 1945;

var bulan_string;

switch (bulan) {
  case 1: {
    bulan_string = "Januari";
    break;
  }
  case 2: {
    bulan_string = "Februari";
    break;
  }
  case 3: {
    bulan_string = "Maret";
    break;
  }
  case 4: {
    bulan_string = "April";
    break;
  }
  case 5: {
    bulan_string = "Mei";
    break;
  }
  case 6: {
    bulan_string = "Juni";
    break;
  }
  case 7: {
    bulan_string = "Juli";
    break;
  }
  case 8: {
    bulan_string = "Agustus";
    break;
  }
  case 9: {
    bulan_string = "September";
    break;
  }
  case 10: {
    bulan_string = "Oktober";
    break;
  }
  case 11: {
    bulan_string = "November";
    break;
  }
  case 12: {
    bulan_string = "Desember";
    break;
  }
  default: {
    bulan_string = "Belum diisi!";
  }
}

console.log(hari + " " + bulan_string + " " + tahun);
