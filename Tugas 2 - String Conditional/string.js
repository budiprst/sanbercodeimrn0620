console.log("Soal 1:");
// Soal 1
var word = "JavaScript";
var second = "is";
var third = "awesome";
var fourth = "and";
var fifth = "I";
var sixth = "love";
var seventh = "it!";

var hasil = word
  .concat(" ")
  .concat(second)
  .concat(" ")
  .concat(third)
  .concat(" ")
  .concat(third)
  .concat(" ")
  .concat(fourth)
  .concat(" ")
  .concat(fifth)
  .concat(" ")
  .concat(sixth)
  .concat(" ")
  .concat(seventh);

console.log(hasil);

console.log(" ");
console.log("Soal 2:");
//Soal 2
var sentence = "I am going to be React Native Developer";
var exampleFirstWord = sentence[0];
var exampleSecondWord = sentence[2] + sentence[3];
var thirdWord = sentence.substr(5, 5); // lakukan sendiri
var fourthWord = sentence.substr(11, 2); // lakukan sendiri
var fifthWord = sentence.substr(14, 2); // lakukan sendiri
var sixthWord = sentence.substr(17, 5); // lakukan sendiri
var seventhWord = sentence.substr(30, 9); // lakukan sendiri
console.log("First Word: " + exampleFirstWord);
console.log("Second Word: " + exampleSecondWord);
console.log("Third Word: " + thirdWord);
console.log("Fourth Word: " + fourthWord);
console.log("Fifth Word: " + fifthWord);
console.log("Sixth Word: " + sixthWord);
console.log("Seventh Word: " + seventhWord);

console.log(" ");
console.log("Soal 3:");
//Soal 3
var sentence2 = "wow JavaScript is so cool";
var exampleFirstWord2 = sentence2.substring(0, 3);
var secondWord2 = sentence2.substring(4, 14); // do your own!
var thirdWord2 = sentence2.substring(15, 17); // do your own!
var fourthWord2 = sentence2.substring(18, 20); // do your own!
var fifthWord2 = sentence2.substring(21, 25); // do your own!
console.log("First Word: " + exampleFirstWord2);
console.log("Second Word: " + secondWord2);
console.log("Third Word: " + thirdWord2);
console.log("Fourth Word: " + fourthWord2);
console.log("Fifth Word: " + fifthWord2);

console.log(" ");
console.log("Soal 4:");
//Soal 4
var sentence3 = "wow JavaScript is so cool";
var exampleFirstWord3 = sentence3.substring(0, 3);
var secondWord3 = sentence3.substring(4, 14); // do your own!
var thirdWord3 = sentence3.substring(15, 17); // do your own!
var fourthWord3 = sentence3.substring(18, 20); // do your own!
var fifthWord3 = sentence3.substring(21, 25); // do your own!

var firstWordLength = exampleFirstWord3.length;
var secondWordLength = secondWord3.length;
var thirdWordLength = thirdWord3.length;
var fourthWordLength = fourthWord3.length;
var fifthWordLength = fifthWord3.length;
// lanjutkan buat variable lagi di bawah ini
console.log(
  "First Word: " + exampleFirstWord3 + ", with length: " + firstWordLength
);
console.log(
  "Second Word: " + secondWord3 + ", with length: " + secondWordLength
);
console.log("Third Word: " + thirdWord3 + ", with length: " + thirdWordLength);
console.log(
  "Fourth Word: " + fourthWord3 + ", with length: " + fourthWordLength
);
console.log("Fifth Word: " + fifthWord3 + ", with length: " + fifthWordLength);
