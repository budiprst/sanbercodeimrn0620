// di index.js
var readBooks = require("./callback.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 6000 },
];

// Tulis code untuk memanggil function readBooks di sini
function cb(sisaWaktu, counter) {
  if (counter <= books.length - 1) {
    if (sisaWaktu >= books[counter].timeSpent) {
      readBooks(sisaWaktu, books[counter], function (w) {
        cb(w, counter + 1);
      });
    } else {
      readBooks(sisaWaktu, books[counter], function (w) {
        return;
      });
    }
  }
}

cb(10000, 0);
