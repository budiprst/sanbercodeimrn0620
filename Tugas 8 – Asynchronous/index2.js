var readBooksPromise = require("./promise.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 6000 },
];

// Lanjutkan code untuk menjalankan function readBooksPromise
function cb(sisaWaktu, counter) {
  if (counter <= books.length - 1) {
    if (sisaWaktu >= books[counter].timeSpent) {
      readBooksPromise(sisaWaktu, books[counter]).then(function (w) {
        cb(w, counter + 1);
      });
    } else {
      readBooksPromise(sisaWaktu, books[counter])
        .then(function (w) {
          return;
        })
        .catch(function (w) {
          return;
        });
    }
  }
}

cb(10000, 0);

/* var i = 0;

readBooksPromise(10000, books[i]).then(function (e) {
  readBooksPromise(e, books[i + 1]).then(function (ee) {
    readBooksPromise(ee, books[i + 2]).then(function (eee) {
      readBooksPromise(eee, books[i + 3])
        .then(function (eeee) {
          return eeee;
        })
        .catch(function (err) {
          console.log(err);
        });
    });
  });
});
 */
