import { StatusBar } from "expo-status-bar";
import React from "react";
import { StyleSheet, Text, View } from "react-native";
//import App from "./Tugas/Tugas12/App";
//import LoginScreen from "./Tugas/Tugas13/LoginScreen";
//import AboutScreen from "./Tugas/Tugas13/AboutScreen";
//import App from "./Tugas/Tugas14/App";
//import SkillScreen from "./Tugas/Tugas14/SkillScreen";
//import App from "./Tugas/Tugas15/index";
import App from "./Tugas/Quiz3/index";

export default function AppIndex() {
  return <App />;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
