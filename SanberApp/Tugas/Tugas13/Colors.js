export default {
  lightBlue: "#3EC6FF",
  darkBlue: "#003366",
  grayBackground: "#EFEFEF",
  blueBackground: "#B4E9FF",
};
