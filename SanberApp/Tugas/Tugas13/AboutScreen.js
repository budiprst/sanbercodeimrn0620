import React, { Component } from "react";
import {
  View,
  StyleSheet,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
} from "react-native";
import Colors from "./Colors";
//import Icon from "react-native-vector-icons/Ionicons";
import { Ionicons, MaterialCommunityIcons } from "@expo/vector-icons";

export default class AboutScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.separoAtas}>
          <View style={styles.judulCont}>
            <Text style={styles.judul}>Tentang Saya</Text>
          </View>
          <View style={styles.avatarCont}>
            <View style={styles.iconCont}>
              <Ionicons
                name="md-person"
                size={150}
                //color={Colors.grayBackground}
                style={styles.icon}
              />
            </View>
          </View>
          <View style={styles.detailCont}>
            <View style={styles.textCont}>
              <Text style={styles.textNama}>Budi Prasetyo</Text>
              <Text style={styles.textJob}>React Native Developer</Text>
            </View>
          </View>
        </View>
        <View style={styles.separoBawah}>
          <View style={styles.portfolio}>
            <View style={styles.bawahHead}>
              <Text>Portfolio</Text>
            </View>
            <View style={styles.bawahDet}>
              <View style={styles.portIsi}>
                <MaterialCommunityIcons
                  name="gitlab"
                  size={50}
                  style={styles.iconPort}
                />
                <Text style={styles.bawahText}>@budiprst</Text>
              </View>
              <View style={styles.portIsi}>
                <MaterialCommunityIcons
                  name="github-circle"
                  size={50}
                  style={styles.iconPort}
                />
                <Text style={styles.bawahText}>@budiprst</Text>
              </View>
            </View>
          </View>
          <View style={styles.hubungiSaya}>
            <View style={styles.bawahHead}>
              <Text>Hubungi Saya</Text>
            </View>
            <View style={styles.bawahDet2}>
              <View style={styles.portIsi2}>
                <MaterialCommunityIcons
                  name="facebook-box"
                  size={50}
                  style={styles.iconPort}
                />
                <Text style={styles.bawahText}>@budiprst</Text>
              </View>
              <View style={styles.portIsi2}>
                <MaterialCommunityIcons
                  name="instagram"
                  size={50}
                  style={styles.iconPort}
                />
                <Text style={styles.bawahText}>@budiprst</Text>
              </View>
              <View style={styles.portIsi2}>
                <MaterialCommunityIcons
                  name="twitter"
                  size={50}
                  style={styles.iconPort}
                />
                <Text style={styles.bawahText}>@budiprst</Text>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "stretch",
  },
  separoAtas: {
    flex: 1,
    justifyContent: "center",
    alignItems: "stretch",
    //backgroundColor: "#fffc0a",
  },
  separoBawah: {
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "stretch",
  },
  judulCont: {
    flex: 1,
    justifyContent: "flex-end",
    alignItems: "center",
    //backgroundColor: "#f34c0a",
  },
  judul: {
    fontSize: 30,
    fontWeight: "bold",
    color: Colors.darkBlue,
  },
  avatarCont: {
    flex: 3,
    paddingVertical: 20,
    alignItems: "center",
    //backgroundColor: Colors.grayBackground,
  },
  iconCont: {
    width: 200,
    height: 200,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 100,
    backgroundColor: Colors.grayBackground,
  },
  icon: {
    justifyContent: "center",
    alignSelf: "center",
    //backgroundColor: "#efc234",
    color: "grey",
  },
  detailCont: {
    flex: 1,
    alignItems: "stretch",
    //backgroundColor: "#e5fd2a",
  },
  textCont: {
    alignItems: "center",
  },
  textNama: {
    fontSize: 25,
    fontWeight: "bold",
    color: Colors.darkBlue,
  },
  textJob: {
    fontSize: 12,
    fontWeight: "bold",
    color: Colors.lightBlue,
  },
  portfolio: {
    flex: 1,
    position: "relative",
    margin: 10,
    backgroundColor: Colors.grayBackground,
    borderRadius: 20,
    paddingHorizontal: 20,
  },
  hubungiSaya: {
    flex: 2,
    position: "relative",
    margin: 10,
    backgroundColor: Colors.grayBackground,
    borderRadius: 20,
    paddingVertical: 5,
    paddingHorizontal: 20,
  },
  bawahHead: {
    borderBottomColor: "black",
    borderBottomWidth: 1,
  },
  bawahDet: {
    flexDirection: "row",
    paddingVertical: 10,
    justifyContent: "space-around",
    alignItems: "stretch",
  },
  bawahDet2: {
    flex: 1,
    flexDirection: "column",
    paddingTop: 10,
    justifyContent: "space-evenly",
    alignItems: "stretch",
  },
  portIsi: {
    flex: 1,
    flexDirection: "column",
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
    paddingBottom: 20,
    //backgroundColor: "#3c3c66",
  },
  iconPort: {
    justifyContent: "center",
    alignSelf: "center",
    color: Colors.lightBlue,
  },
  portIsi2: {
    flex: 1,
    flexDirection: "row",
    height: 60, //"100%",
    justifyContent: "center",
    alignItems: "center",
    //paddingBottom: 60,
    //backgroundColor: "#3c3c66",
    //borderBottomWidth: 1,
  },
  bawahText: {
    fontWeight: "bold",
    color: Colors.darkBlue,
  },
});
