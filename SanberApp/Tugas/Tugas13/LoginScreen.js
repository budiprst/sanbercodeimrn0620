import React, { Component } from "react";
import {
  View,
  StyleSheet,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
} from "react-native";
import Colors from "./Colors";

class LoginScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.logoCont}>
          <View style={styles.imgCont}>
            <Image source={require("./images/logo.png")} style={styles.logo} />
          </View>
          <View style={styles.formTitle}>
            <Text style={styles.title}>Login</Text>
          </View>
        </View>
        <View style={styles.formCont}>
          <View style={styles.formContent}>
            <Text style={styles.formContentTitle}>Username/Email</Text>
            <TextInput style={styles.formContentInput} />
          </View>
          <View style={styles.formContent}>
            <Text style={styles.formContentTitle}>Password</Text>
            <TextInput style={styles.formContentInput} />
          </View>
        </View>
        <View style={styles.footerCont}>
          <TouchableOpacity style={styles.buttonCont}>
            <View style={styles.buttonMasuk}>
              <Text style={styles.buttontext}>Masuk</Text>
            </View>
          </TouchableOpacity>
          <Text></Text>
          <Text style={styles.atau}>atau</Text>
          <Text></Text>
          <TouchableOpacity style={styles.buttonCont}>
            <View style={styles.buttonDaftar}>
              <Text style={styles.buttontext}>Daftar ?</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "stretch",
  },
  logoCont: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "stretch",
    //backgroundColor: "#00ff00",
  },
  formCont: {
    flex: 0.5,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "stretch",
    paddingHorizontal: "10%",
    paddingVertical: "5%",
    //backgroundColor: "#ffff00",
  },
  footerCont: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "center",
    paddingTop: 30,
    //backgroundColor: "#00cc3c",
  },

  imgCont: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    padding: "20%",
  },
  logo: {
    height: 200,
    width: 400,
    resizeMode: "contain",
  },
  formTitle: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "flex-end",
    alignItems: "center",
    paddingBottom: 20,
    //backgroundColor: "#f45b00",
  },
  title: {
    fontSize: 20,
    fontWeight: "bold",
    color: Colors.darkBlue,
  },
  formContent: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "stretch",
    //backgroundColor: "#0ffccc",
  },
  formContentTitle: {
    fontSize: 12,
    fontWeight: "bold",
    color: Colors.darkBlue,
  },
  formContentInput: {
    width: "100%",
    height: "45%",
    borderWidth: 1,
    borderColor: Colors.darkBlue,
  },
  buttonCont: {
    alignItems: "center",
    width: "35%",
  },
  buttonMasuk: {
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
    height: 30,
    //borderWidth: 1,
    //borderColor: Colors.darkBlue,
    borderRadius: 12,
    backgroundColor: Colors.lightBlue,
  },
  buttonDaftar: {
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
    height: 30,
    //borderWidth: 1,
    //borderColor: Colors.darkBlue,
    borderRadius: 12,
    backgroundColor: Colors.darkBlue,
  },
  atau: {
    color: Colors.lightBlue,
  },
  buttontext: {
    color: "#fff",
  },
});

export default LoginScreen;
