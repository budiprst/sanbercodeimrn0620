import React from "react";
import { View, StyleSheet, Text } from "react-native";
import Colors from "../Tugas13/Colors";
import { MaterialCommunityIcons } from "@expo/vector-icons";

function SkillItem({ skillName, categoryName, percentage, iconName }) {
  return (
    <View style={styles.container}>
      <View style={styles.column0}>
        <View style={styles.column0_icon}>
          <MaterialCommunityIcons
            name={iconName}
            size={70}
            style={styles.icon}
          />
        </View>
      </View>
      <View style={styles.column1}>
        <Text style={styles.column1_name}>{skillName}</Text>
        <Text style={styles.column1_cat}>{categoryName}</Text>
        <Text style={styles.column1_percent}>{percentage}</Text>
      </View>
      <View style={styles.column2}>
        <MaterialCommunityIcons
          name="chevron-right"
          size={70}
          style={styles.icon}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    backgroundColor: Colors.blueBackground,
    width: "100%",
    margin: 5,
    padding: 5,
    borderRadius: 10,
    elevation: 10,
    //borderWidth: 1,
  },
  column0: {
    flex: 1,
    justifyContent: "center",
    alignItems: "stretch",
    //backgroundColor: "#00000055",
  },
  column1: {
    flex: 2,
  },
  column1_name: {
    fontWeight: "bold",
    fontSize: 25,
    color: Colors.darkBlue,
  },
  column1_cat: {
    //fontWeight: "bold",
    fontSize: 15,
    color: Colors.lightBlue,
  },
  column1_percent: {
    fontWeight: "bold",
    fontSize: 35,
    color: "white",
    alignSelf: "flex-end",
  },
  column2: {
    flex: 1,
    justifyContent: "center",
    alignItems: "flex-end",
  },
  column0_icon: {
    flex: 1,
    justifyContent: "center",
    alignItems: "stretch",
    padding: 5,
    //backgroundColor: "#ff000055",
  },
  icon: {
    color: Colors.darkBlue,
    //borderWidth: 1,
  },
});

export default SkillItem;
