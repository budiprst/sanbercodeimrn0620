import React, { Component } from "react";
import {
  View,
  StyleSheet,
  Image,
  Text,
  TouchableOpacity,
  ScrollView,
  FlatList,
} from "react-native";
import Colors from "../Tugas13/Colors";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import skill from "./skillData.json";
import SkillItem from "./SkillItem";

class SkillScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.row1}>
          <View style={styles.row1_logoCont}></View>
          <View style={styles.row1_logoCont}>
            <Image
              source={require("../Tugas13/images/logo.png")}
              style={styles.logo}
            />
          </View>
        </View>
        <View style={styles.row2}>
          <View style={styles.row2_avatarBack}>
            <MaterialCommunityIcons
              name="account"
              size={50}
              style={styles.iconAvatar}
            />
          </View>
          <View style={styles.row2_detail}>
            <Text style={styles.row2_detail_hai}>Hai,</Text>
            <Text style={styles.row2_detail_nama}>Budi Prasetyo</Text>
          </View>
        </View>
        <View style={styles.row3}>
          <Text style={styles.row3_text}>SKILL</Text>
        </View>
        <View style={styles.row4}>
          <ScrollView horizontal showsHorizontalScrollIndicator={false}>
            <TouchableOpacity>
              <View style={styles.row4_button}>
                <Text style={styles.row4_text}>Library / Framework</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity>
              <View style={styles.row4_button}>
                <Text style={styles.row4_text}>Bahasa Pemrograman</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity>
              <View style={styles.row4_button}>
                <Text style={styles.row4_text}>Teknologi</Text>
              </View>
            </TouchableOpacity>
          </ScrollView>
        </View>
        <View style={styles.row5}>
          <FlatList
            data={skill.items}
            renderItem={(skill) => (
              <View>
                <SkillItem
                  skillName={skill.item.skillName}
                  categoryName={skill.item.categoryName}
                  percentage={skill.item.percentageProgress}
                  iconName={skill.item.iconName}
                />
              </View>
            )}
            keyExtractor={(item) => "" + item.id}
            ItemSeparatorComponent={() => (
              <View style={{ height: 0.5, backgroundColor: "#e5e5e5" }} />
            )}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: "3%",
  },
  row1: {
    //flex: 1,
    flexDirection: "row",
    height: "10%",
    justifyContent: "center",
    alignItems: "stretch",
    //backgroundColor: "#00aa04",
  },
  row2: {
    //flex: 1,
    height: "8%",
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    //backgroundColor: "#33aa04",
  },
  row3: {
    //flex: 1,
    height: "8%",
    justifyContent: "flex-end",
    alignItems: "flex-start",
    borderBottomWidth: 3,
    borderBottomColor: Colors.lightBlue,
    //backgroundColor: "#55aa04",
  },
  row4: {
    //flex: 1,
    flexDirection: "row",
    height: "10%",
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: 5,
    //backgroundColor: "#88aa04",
  },
  row5: {
    flex: 1,
    justifyContent: "center",
    alignItems: "stretch",
    //backgroundColor: "#aaaa04",
  },
  row1_logoCont: {
    flex: 1,
    height: "100%",
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
    //backgroundColor: "red",
    //borderWidth: 1,
  },
  logo: {
    width: 200,
    height: 100,
    resizeMode: "contain",
    //alignSelf: "flex-end",
  },
  row2_avatarBack: {
    height: 50,
    width: 50,
    borderRadius: 25,
    backgroundColor: Colors.lightBlue,
  },
  iconAvatar: {
    //backgroundColor: Colors.lightBlue,
    color: "white",
  },
  row2_detail: {
    height: 50,
    width: "100%",
    paddingHorizontal: 10,
    //backgroundColor: "#f12345",
    //borderWidth: 1,
  },
  row2_detail_hai: {
    fontSize: 15,
  },
  row2_detail_nama: {
    fontSize: 20,
    color: Colors.darkBlue,
  },
  row3_text: {
    fontSize: 40,
    color: Colors.darkBlue,
  },
  row4_button: {
    backgroundColor: Colors.blueBackground,
    padding: 13,
    borderRadius: 13,
  },
  row4_text: {
    fontSize: 15,
    fontWeight: "bold",
    color: Colors.darkBlue,
  },
});

export default SkillScreen;
