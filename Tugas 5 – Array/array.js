console.log("");
console.log("Soal 1");
//Soal 1

function range(startNum, finishNum) {
  if (startNum && finishNum) {
    var hasil = [];
    if (startNum <= finishNum) {
      for (var i = startNum; i <= finishNum; i++) {
        hasil.push(i);
        //console.log(i);
      }
    } else {
      for (var i = startNum; i >= finishNum; i--) {
        hasil.push(i);
        //console.log(i);
      }
    }
  } else {
    var hasil = -1;
  }

  return hasil;
}

console.log(range(1, 10)); //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)); // -1
console.log(range(11, 18)); // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)); // [54, 53, 52, 51, 50]
console.log(range()); // -1

console.log("");
console.log("Soal 2");
//Soal 2

function rangeWithStep(startNum, finishNum, step) {
  if (startNum && finishNum) {
    var hasil = [];
    if (startNum <= finishNum) {
      var i = startNum;
      while (i <= finishNum) {
        hasil.push(i);
        //console.log(i);
        i += step;
      }
    } else {
      var i = startNum;
      while (i >= finishNum) {
        hasil.push(i);
        //console.log(i);
        i -= step;
      }
    }
  } else {
    var hasil = -1;
  }

  return hasil;
}

console.log(rangeWithStep(1, 10, 2)); // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)); // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)); // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)); // [29, 25, 21, 17, 13, 9, 5]

console.log("");
console.log("Soal 3");
//Soal 3

function sum(startNum, finishNum, step) {
  if (!startNum) {
    return 0;
  }
  if (!finishNum) {
    return startNum;
  }
  if (!step) {
    step = 1;
  }
  var res = rangeWithStep(startNum, finishNum, step);
  var total = 0;
  res.forEach((element) => {
    total += element;
    //console.log(element);
  });

  return total;
}
console.log(sum(1, 10)); // 55
console.log(sum(5, 50, 2)); // 621
console.log(sum(15, 10)); // 75
console.log(sum(20, 10, 2)); // 90
console.log(sum(1)); // 1
console.log(sum()); // 0

console.log("");
console.log("Soal 4");
//Soal 4

function dataHandling(params) {
  params.forEach((e) => {
    console.log("Nomor ID: " + e[0]);
    console.log("Nama Lengkap: " + e[1]);
    console.log("TTL: " + e[2] + " " + e[3]);
    console.log("Hobi: " + e[4]);
    console.log("");
  });
}

//contoh input
var input = [
  ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
  ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
  ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
  ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"],
];

dataHandling(input);

console.log("");
console.log("Soal 5");
//Soal 5

function balikKata(str) {
  //console.log(str.length);
  var max = str.length - 1;
  var hasil = "";
  for (var i = max; i >= 0; i--) {
    hasil += str[i];
    //console.log(str[i]);
  }
  return hasil;
}

console.log(balikKata("Kasur Rusak")); // kasuR rusaK
console.log(balikKata("SanberCode")); // edoCrebnaS
console.log(balikKata("Haji Ijah")); // hajI ijaH
console.log(balikKata("racecar")); // racecar
console.log(balikKata("I am Sanbers")); // srebnaS ma I

console.log("");
console.log("Soal 6");
//Soal 6

function getBulan(bulan) {
  var bulan_string;
  switch (bulan) {
    case "01": {
      bulan_string = "Januari";
      break;
    }
    case "02": {
      bulan_string = "Februari";
      break;
    }
    case "03": {
      bulan_string = "Maret";
      break;
    }
    case "04": {
      bulan_string = "April";
      break;
    }
    case "05": {
      bulan_string = "Mei";
      break;
    }
    case "06": {
      bulan_string = "Juni";
      break;
    }
    case "07": {
      bulan_string = "Juli";
      break;
    }
    case "08": {
      bulan_string = "Agustus";
      break;
    }
    case "09": {
      bulan_string = "September";
      break;
    }
    case "10": {
      bulan_string = "Oktober";
      break;
    }
    case "11": {
      bulan_string = "November";
      break;
    }
    case "12": {
      bulan_string = "Desember";
      break;
    }
    default: {
      bulan_string = "Belum diisi!";
    }
  }
  return bulan_string;
}

function dataHandling2(params) {
  //console.log(params);
  resArr = params.splice(0, 4);
  resArr.splice(4, 0, "Membaca");

  //edit nama
  resNama = resArr[1];
  resNama = resNama.slice(0, 15);
  //console.log(resNama);
  resArr.splice(1, 1, resNama);

  //edit alamat
  resAlamat = resArr[2];
  resAlamat = resAlamat.slice(9);
  //console.log(resAlamat);
  resArr.splice(2, 1, resAlamat);

  console.log(resArr); //output 1

  resTanggal = resArr[3];
  resTglArr = resTanggal.split("/");
  console.log(getBulan(resTglArr[1])); //output 2

  resTglTemp = resTglArr[2];
  resTglArr2 = [];
  resTglArr2.push(resTglTemp);
  resTglArr2.push(resTglArr[0]);
  resTglArr2.push(resTglArr[1]);
  console.log(resTglArr2); //output 3

  console.log(resTglArr[0] + "-" + resTglArr[1] + "-" + resTglArr[2]); //output 4

  console.log(resNama); //output 5
}

var input = [
  "0001",
  "Roman Alamsyah Elsharawy",
  "Provinsi Bandar Lampung",
  "21/05/1989",
  "Pria",
  "SMA Internasional Metro",
];

dataHandling2(input);
